//! # Map-Maker
//!
//! ## Problem Overview
//! John is a map maker. He is standing at point (0,0) and must explore the area. John can move one space at a time: left, right, up, or down. If John is standing at (x,y), he can move to (x+1, y), (x-1, y), (x, y+1), or (x, y-1). 
//!
//! Bill is an assassin, and must stop John from exploring. Therefore, Bill decided to plant mines. Bill has placed mines at points where the sum of the absolute value of the coordinate's digits is greater than 21. For example, the point (59, -79) has a mine, because 5 + 9 + 7 + 9 > 21. But the point (-113, -104) has no mine because 1 + 1 + 3 + 1 + 0 + 4 <= 21.
//!
//! If John steps on a mine he will die. John cannot jump over the mines, so he must walk around the mines to avoid them. How many points can John access while making his map, given that he starts at (0, 0).
//!
//! ## Assumptions
//! Given the digit sum is the absolute value of the coordinate system, we can simply calculate the number of valid points in one quadrant and multiply that value by 4. We will then remove the overlapping points (those at the axis) to get our final answer.
//! 
//! Our program will use unsigned values, and perform calculations in Quadrant I.

#![feature(test)]
#[allow(dead_code)]
extern crate test;
use std::env;
use std::collections::HashSet;

/// Returns the number of valid, accessible points in Quadrant I, as well as the calculated number of valid, accessible points for all four quadrants (final answer).
fn find_valid_points(size: usize, constraint: usize) -> (usize, usize) {
    let mut valid_cells = HashSet::new();
    
    // Places valid, traversible points into a set, set.len() keeps track of # valid points.
    for x in 0..size {
        for y in 0..size {
            if sum_of_digits(x, y) <= constraint {
                if x == 0 || y == 0 {
                    valid_cells.insert(size * x + y);
                } 
                else if valid_cells.contains(&(size * (x-1) + y)) ||
                        valid_cells.contains(&(size * x + (y-1))) {
                    valid_cells.insert(size * x + y);
                }
            }
        }
    }
    
    // Calculate all traversible points for four quadrants
    let raw_count = valid_cells.len();
    let count = (raw_count * 4) - (size * 4) + 1;
    return (raw_count, count);
}

/// Returns the sum of digits,`s`, with x-coord `x` and y-coord `y`
/// ### Examples
/// ```
/// let x = 59;
/// let y = 79;
/// assert_eq!(30, sum_of_digits(x, y));
/// ```
fn sum_of_digits(x: usize, y: usize) -> usize {
    let mut sum = 0;
    let mut _x = x;
    let mut _y = y;
    
    while _x != 0 {
        sum = sum + _x % 10;
        _x /= 10;
    }
    while _y != 0 {
        sum = sum + _y % 10;
        _y /= 10;
    }
    sum
}

/// Returns the smallest excluding number,`n`, with given sum of digits `c`
/// `n-1` must be visitable from the origin (0,0).
/// ### Examples
/// ```
/// let twenty_one = 21;
/// assert_eq!(499, get_max_height(twenty_one));
/// ```

fn get_max_height(c : usize) -> usize {
    let mut val = 0;
    while sum_of_digits(val, 0) < c || (val % 10 == 9) {
        val += 1;
    }
    val+1
}

/// Main driver
/// ### Usage: 
/// `cargo run`, `cargo run main [constraint]`
/// `constraint (u32)`: defines the minimum sum of the absolute value of the coordinate's digits that has a mine (default: 21).");

fn main() {
    let args:Vec<String> = env::args().collect();
    let mut constraint = 21;
    
    if args.len() > 2 {
        println!("Too many arguments.\n
                  Usage: cargo run, cargo run [constraint] \n
                  \t constraint (u32): defines minimum sum of the absolute value of the coordinate's digits that has a mine (default: 21).");
    } else if args.len() == 2 {
        constraint = args[1].parse::<usize>().unwrap();
    }
    
    let size = get_max_height(constraint);
    // println!("Max height: {}", size);
    
    let (raw_count, count) = find_valid_points(size, constraint);
    
    // println!("Raw count: {}, Final count: {}", raw_count, count);
    println!("John can safely access {} points on the map, given that he starts at (0,0).", count);

}

#[cfg(test)]
mod tests {
    use test::Bencher;

    #[test]
    fn test_sum_of_digits() {
        use sum_of_digits;

        let x = 59;
        let y = 79; 
        assert_eq!(30, sum_of_digits(x, y));
        
        let x = 113;
        let y = 104;
        assert_eq!(10, sum_of_digits(x,y))
    }
    #[test]
    fn test_get_max_height() {
        use get_max_height;
        
        let twenty_one = 21;
        assert_eq!(499, get_max_height(twenty_one));
        
        let ten = 10;
        assert_eq!(29, get_max_height(ten));
    }
    #[test]
    fn test_find_valid_points() {
        use find_valid_points;
        use get_max_height;
        
        let constraint = 0;
        let size = get_max_height(constraint);
        assert_eq!((1,1), find_valid_points(size, constraint));
        
        let constraint = 4;
        let size = get_max_height(constraint);
        assert_eq!((15, 41), find_valid_points(size, constraint));
        
        let constraint = 5;
        let size = get_max_height(constraint);
        assert_eq!((21,61), find_valid_points(size, constraint));
        
        let constraint = 10;
        let size = get_max_height(constraint);
        assert_eq!((309,1121), find_valid_points(size, constraint));
        
        let constraint = 21;
        let size = get_max_height(constraint);
        assert_eq!((72469,287881), find_valid_points(size, constraint));
    }
    #[bench]
    fn bench_sum_of_digits(b: &mut Bencher) {
        use sum_of_digits;
        
        let x = 59;
        let y = 79;
        b.iter(|| sum_of_digits(x,y));
    }
    #[bench]
    fn bench_get_max_height(b: &mut Bencher) {
        use get_max_height;
        
        let twenty_one = 21;
        b.iter(|| get_max_height(twenty_one));
    }
    #[bench]
    fn bench_find_valid_points(b: &mut Bencher) {
        use find_valid_points;
        
        let constraint = 21;
        b.iter(|| find_valid_points(499, constraint));
    }
}